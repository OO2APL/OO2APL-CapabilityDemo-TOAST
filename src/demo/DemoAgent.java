package demo;
 
import java.io.IOException;
import java.util.UUID;

import oo2apl.agent.AgentBuilder;
import oo2apl.agent.AgentContextInterface;
import oo2apl.agent.Trigger;
import oo2apl.plan.builtin.SubPlanInterface;
import toastCapability.ArgumentationTheory;
import toastCapability.ToastCapability;
import toastCapability.ToastContext;
/**
 * This DemoAgent can read a theory from the file 'myTheory.txt' and show its TOAST output. 
 * 
 * For your own application you can specify the theory either programmatically or via a text file. After 
 * which you can process the output from TOAST for your purposes.
 *   
 * @author Bas Testerink
 */
public final class DemoAgent extends AgentBuilder {
	
	public DemoAgent(){
		// Connect to TOAST, note; you can host toast yourself and replace the URL here
		super.include(new ToastCapability("http://www.arg.dundee.ac.uk/toast/api/evaluate"));
		
		// Add the scheme to handle the reload trigger
		addExternalTriggerPlanScheme(DemoAgent::handleReloadScheme);
	} 
	
	/** Add a scheme that upon the reload trigger reloads the theory file and shows the TOAST output. */
	private static final SubPlanInterface handleReloadScheme(final Trigger trigger, final AgentContextInterface contextInterface){
		if(trigger instanceof ReloadFileTrigger){
			return (planInterface)->{
				// Get the context that handles the toast interface
				ToastContext toast = planInterface.getContext(ToastContext.class);
				try {  
					// Load the file
					UUID theoryID = toast.newArgumentationTheoryFromFile("myTheory.txt");
					ArgumentationTheory theory = toast.getArgumentationTheory(theoryID);
					// Evaluate and output using TOAST
					System.out.println(theory.getTOASTOutput());
					// Clear the theory (you can also keep it and modify it later, if needed)
					toast.removeArgumentationTheory(theoryID);
				} catch (IOException e) { 
					e.printStackTrace();
				}
			};
		} else return SubPlanInterface.UNINSTANTIATED;
	}
}
