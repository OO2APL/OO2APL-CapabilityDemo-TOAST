package demo;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import oo2apl.agent.ExternalProcessToAgentInterface;
import oo2apl.defaults.messenger.DefaultMessenger;
import oo2apl.platform.AdminToPlatformInterface;
import oo2apl.platform.Platform;
/**
 * This demo shows how you can specify an argumentation theory in a file and load it into the 
 * agent for evaluation. 
 * 
 * @author Bas Testerink
 */
public final class ToastDemoMain {
	// User input
	private final Scanner scanner;
	private final ExecutorService executor;

	// The agent platform
	private final AdminToPlatformInterface platform;
	private final ExternalProcessToAgentInterface agent;

	public ToastDemoMain(){ 
		// Make the agent
		this.platform = Platform.newPlatform(1, new DefaultMessenger());
		this.agent = platform.newAgent(new DemoAgent());

		// Make the console connection
		this.scanner = new Scanner(System.in);
		this.executor = Executors.newSingleThreadExecutor(); 
		this.executor.submit(() -> { 
			System.out.println("Type \"reload\" to reload \'myTheory.txt\' and output the corresponding TOAST output, or type \"halt\" to end.\r\n");
			boolean exit = false; 
			while(!exit){ 
				String input = this.scanner.nextLine(); 
				try {
					if(input.equalsIgnoreCase("halt"))
						exit = true;
					else if(input.equalsIgnoreCase("reload"))
						this.agent.addExternalTrigger(new ReloadFileTrigger()); 
					else System.out.println("Command not recognized. Use \"reload\" to reload the file or \"halt\" to quit.");
				} catch (Exception e) { 
					exit = true;
					e.printStackTrace();
				}
			}
			this.executor.shutdown();
			this.platform.haltPlatform();
		});
	} 
	
	public static final void main(final String[] args){
		new ToastDemoMain();
	}
}
